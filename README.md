## Demo
[DEMO](http://samplebank-api.cfapps.io/)
Please find the working demo of the API built using Spring Boot, and the Single Page Application built using Angular.

## Package Structure
1. **RESTful API** - samplebank-api
2. **Single Page Application** - samplebank-webapp