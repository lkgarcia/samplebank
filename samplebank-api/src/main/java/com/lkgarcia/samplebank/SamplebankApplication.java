package com.lkgarcia.samplebank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@EntityScan(
	basePackageClasses = {SamplebankApplication.class, Jsr310JpaConverters.class}
)
@SpringBootApplication
public class SamplebankApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamplebankApplication.class, args);
	}
}