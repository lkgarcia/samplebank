package com.lkgarcia.samplebank.domain.account;

import com.lkgarcia.samplebank.domain.customer.Customer;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.annotations.GenericGenerator;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@Entity
public class Account {
  
  @GenericGenerator(
    name = "accountSequenceGenerator",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
            @Parameter(name = "sequence_name", value = "accountSequence"),
            @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1")
    }
  )
  @Id
  @GeneratedValue(generator = "accountSequenceGenerator")
  private Long id;

  @NotEmpty
  @Column(unique = true)
  private String accountNumber;

  @NotNull
  @Enumerated(EnumType.STRING)
  private AccountType accountType;

  @NotEmpty
  private String currency;

  @NotNull
  @Min(0)
  private BigDecimal balance;

  @NotNull
  @Column(columnDefinition = "TIMESTAMP", name = "CREATE_DATE")
  private LocalDateTime createDate;

  @NotNull
  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "CUSTOMER_ID")
  private Customer customer;

  // @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  // @JoinColumn(name = "ACCOUNT_ID")
  // List<Transaction> transactions;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public AccountType getAccountType() {
    return accountType;
  }

  public void setAccountType(AccountType accountType) {
    this.accountType = accountType;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public LocalDateTime getCreateDate() {
    return createDate;
  }

  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  // public void setTransactions (List<Transaction> transactions) {
  //   this.transactions = transactions;
  // }

  // public List<Transaction> getTransactions () {
  //   return transactions;
  // }
}