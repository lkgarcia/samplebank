package com.lkgarcia.samplebank.domain.account;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
public interface AccountRepository extends JpaRepository<Account, Long> {
  Page<Account> findByCustomerId(Long customerId, Pageable pageRequest);
}