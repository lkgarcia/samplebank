package com.lkgarcia.samplebank.domain.account;


public enum AccountType {
  SAVINGS("1"),
  CURRENT("2");

  private String value;

  AccountType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
}