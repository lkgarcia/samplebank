package com.lkgarcia.samplebank.domain.customer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.GenericGenerator;

import org.hibernate.validator.constraints.NotEmpty;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@Entity
public class Customer {
  
  @GenericGenerator(
    name = "customerSequenceGenerator",
    strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
    parameters = {
            @Parameter(name = "sequence_name", value = "customerSequence"),
            @Parameter(name = "initial_value", value = "1"),
            @Parameter(name = "increment_size", value = "1")
    }
  )
  @Id
  @GeneratedValue(generator = "customerSequenceGenerator")
  private Long id;

  @NotEmpty
  @Column(unique = true)
  private String name;

  @NotEmpty
  @Column(unique = true)  
  private String email;

  @NotNull
  @Column(columnDefinition = "DATE", name = "BIRTH_DATE")
  private LocalDate birthDate;

  @NotNull
  @Column(columnDefinition = "TIMESTAMP")
  private LocalDateTime createDate;

  // @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
  // @JoinColumn(name = "CUSTOMER_ID")
  // List<Account> accounts;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public LocalDateTime getCreateDate() {
    return createDate;
  }

  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  // public void setAccounts (List<Account> accounts) {
  //   this.accounts = accounts;
  // }

  // public List<Account> getAccounts () {
  //   return accounts;
  // }
}