package com.lkgarcia.samplebank.domain.customer;

import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {
  
  public Customer findByName(String name);

  public Customer findByEmail(String email);
}