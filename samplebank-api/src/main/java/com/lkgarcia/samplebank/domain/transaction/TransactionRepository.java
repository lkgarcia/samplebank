package com.lkgarcia.samplebank.domain.transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
  Page<Transaction> findByAccountId(Long accountId, Pageable pageRequest);
}