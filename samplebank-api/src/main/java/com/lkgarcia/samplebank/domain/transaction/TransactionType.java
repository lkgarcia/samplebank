package com.lkgarcia.samplebank.domain.transaction;


public enum TransactionType {
  DEBIT("DR"),
  CREDIT("CR");

  private String value;

  TransactionType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
}