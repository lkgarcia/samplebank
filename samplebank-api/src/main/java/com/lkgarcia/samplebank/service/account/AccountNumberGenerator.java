package com.lkgarcia.samplebank.service.account;

import com.lkgarcia.samplebank.domain.account.AccountType;
import java.security.SecureRandom;
import java.util.Random;
import org.springframework.stereotype.Component;

@Component
public class AccountNumberGenerator {
  private Random random = new SecureRandom();
  private String branchCode = "100"; //TODO: Config file

  /**
   * Example: 100-1-12345
   */
  public String generate(AccountType type) {
    return new StringBuffer(branchCode).append("-").append(type.getValue()).append("-").append(String.format("%05d", random.nextInt(100000))).toString();
  }
}