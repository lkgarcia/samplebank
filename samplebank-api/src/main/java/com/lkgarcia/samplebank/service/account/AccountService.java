package com.lkgarcia.samplebank.service.account;

import java.math.BigDecimal;
import com.lkgarcia.samplebank.domain.account.Account;
import com.lkgarcia.samplebank.domain.transaction.TransactionType;
import com.lkgarcia.samplebank.service.account.exception.AccountNotFoundException;
import com.lkgarcia.samplebank.service.account.model.AccountDto;
import com.lkgarcia.samplebank.service.account.model.AccountCreationDto;
import com.lkgarcia.samplebank.service.customer.exception.CustomerNotFoundException;
import com.lkgarcia.samplebank.service.transaction.exception.InsufficientAccountBalanceException;

import org.springframework.data.domain.Page;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
public interface AccountService {

  /**
   * Retrieve account based on ID.
   */
  public AccountDto getAccount(Long customerId, Long accountId) throws AccountNotFoundException;

  /**
   * Retrieve account entity based on ID.
   */
  public Account getAccountEntity(Long accountId) throws AccountNotFoundException;

  /**
   * Retrieve page list of accounts based on customer ID.
   */
  public Page<AccountDto> getAccounts(Long customerId, int page) throws CustomerNotFoundException;

  /**
   * Create a new account.
   */
  public AccountDto createAccount(Long customerId, AccountCreationDto request) throws CustomerNotFoundException;

  /**
   * Update account balance.
   */
  public void updateBalance(Long accountId, TransactionType transactionType, BigDecimal amount) throws AccountNotFoundException, InsufficientAccountBalanceException;

  /**
   * Identifies if an account exists for a given customer.
   */
  public boolean accountExists(Long customerId, Long accountId);
}