package com.lkgarcia.samplebank.service.account;

import com.lkgarcia.samplebank.service.account.exception.AccountNotFoundException;
import com.lkgarcia.samplebank.service.account.model.AccountDto;
import com.lkgarcia.samplebank.service.account.model.AccountCreationDto;
import com.lkgarcia.samplebank.service.customer.CustomerService;
import com.lkgarcia.samplebank.service.customer.exception.*;
import com.lkgarcia.samplebank.service.common.exception.ResourceNotFoundException;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;
import com.lkgarcia.samplebank.service.transaction.exception.InsufficientAccountBalanceException;
import com.lkgarcia.samplebank.service.common.utils.EntityToDtoConverter;

import java.math.BigDecimal;
import com.lkgarcia.samplebank.domain.account.Account;
import com.lkgarcia.samplebank.domain.account.AccountRepository;
import com.lkgarcia.samplebank.domain.customer.Customer;
import com.lkgarcia.samplebank.domain.transaction.TransactionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@Service
public class AccountServiceImpl implements AccountService  {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private AccountRepository accountRepository;

  @Autowired
  private CustomerService customerService;

  @Autowired
  private AccountNumberGenerator generator;

  private final static int PAGE_SIZE = 5; //TODO: Properties

  
  /**
   * {@inheritDoc}
   */
  public AccountDto getAccount(Long customerId, Long accountId) throws AccountNotFoundException {
    logger.debug("Parameter (customerId): {}", customerId);
    logger.debug("Parameter (accountId): {}", accountId);

    AccountDto result = null;

    try {

      Account entity = accountRepository.findOne(accountId); //TODO: search by account number
      logger.debug("entity: {}", entity);

      if (accountExists(customerId, accountId) && entity != null) { //TODO: Do i need accountExists?
      
        result = AccountDto.wrap(entity);
      
      } else {
        logger.warn("Account ({}) not found.", accountId);
        throw new AccountNotFoundException(accountId);
      }
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.error("Unexpected error occurred.", ex);
      throw new SamplebankException();
    }

    logger.debug("Result (AccountDto): {}", result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  public Account getAccountEntity(Long id) throws AccountNotFoundException {
    logger.debug("Parameter (id): {}", id);

    Account result = null;

    try {
      result = accountRepository.findOne(id);
      logger.debug("result: {}", result);

      if (result == null) {
        logger.warn("Account ({}) not found.", id);
        throw new AccountNotFoundException(id);
      }
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }

    logger.debug("Result (Account): {}", result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  public Page<AccountDto> getAccounts(Long customerId, int page) throws CustomerNotFoundException {
    logger.debug("Parameter (customerId): {}", customerId);
    logger.debug("Parameter (page): {}", page);
    
    Page<AccountDto> result = null;

    try {
      Page<Account> entityList = accountRepository.findByCustomerId(customerId, new PageRequest(page, PAGE_SIZE));
      logger.debug("entityList: {}", entityList);
      
      if (entityList != null && page < entityList.getTotalPages()) {
        result = entityList.map(new EntityToDtoConverter<Account, AccountDto>(AccountDto.class));
      } else {
        logger.warn("Resource (Article) not found.");
        throw new ResourceNotFoundException(Account.class);
      } 
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }

    logger.debug("Result (List<AccountDto>): {}", result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(rollbackFor = Exception.class)
  public AccountDto createAccount(Long customerId, AccountCreationDto request) throws CustomerNotFoundException {
    logger.debug("Parameter (customerId): {}", customerId);
    logger.debug("Parameter (request): {}", request);

    AccountDto result = null;

    try {
      //Check if customer exists
      Customer customer = customerService.getCustomerEntity(customerId);
      
      if (customer != null) {
        //Create account
        Account account = new Account();
        BeanUtils.copyProperties(request, account);
        account.setAccountNumber(generator.generate(account.getAccountType()));
        account.setCustomer(customer);
        
        //Persist
        accountRepository.save(account);
        result = AccountDto.wrap(account);
      } else {
        logger.warn("Customer ({}) not found.", customerId);
        throw new CustomerNotFoundException(customerId);
      }
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }

    logger.debug("Result (AccountDto): {}", result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(rollbackFor=Exception.class)
  public void updateBalance(Long accountId, TransactionType transactionType, BigDecimal amount) throws AccountNotFoundException, InsufficientAccountBalanceException {
    logger.debug("Parameter (accountId): {}", accountId);
    logger.debug("Parameter (transactionType): {}", transactionType);
    logger.debug("Parameter (amount): {}", amount);

    try {
      //Check if account exists
      Account account = accountRepository.findOne(accountId);
      
      if (account != null) {
        //Check balance
        BigDecimal newBalance =  (transactionType == TransactionType.DEBIT) ? account.getBalance().subtract(amount) : account.getBalance().add(amount);
        if (newBalance.compareTo(BigDecimal.ZERO) >= 0) {
          //Update account balance
          account.setBalance(newBalance);

          //Persist
          accountRepository.save(account);
        } else {
          throw new InsufficientAccountBalanceException(account.getBalance(), amount);
        }
        
      } else {
        logger.warn("Account ({}) not found.", accountId);
        throw new AccountNotFoundException(accountId);
      }
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }
  }

  /**
   * {@inheritDoc}
   */
  public boolean accountExists(Long customerId, Long accountId) {
    return true; //TODO
  }
}