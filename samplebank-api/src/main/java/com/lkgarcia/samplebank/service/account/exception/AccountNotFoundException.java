package com.lkgarcia.samplebank.service.account.exception;

import com.lkgarcia.samplebank.service.common.exception.ErrorCode;
import com.lkgarcia.samplebank.service.common.exception.ErrorResponse;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;

import org.springframework.http.HttpStatus;

public class AccountNotFoundException extends SamplebankException {

  private static final long serialVersionUID = 2807400529807999585L;

  public AccountNotFoundException(Long id) {
    super(HttpStatus.NOT_FOUND, new ErrorResponse(ErrorCode.NOT_FOUND, new StringBuffer("Account (").append(id).append(") not found.").toString())); //TODO: Error message from property
  }
}