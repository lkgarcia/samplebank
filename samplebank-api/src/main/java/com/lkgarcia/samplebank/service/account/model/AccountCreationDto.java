package com.lkgarcia.samplebank.service.account.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lkgarcia.samplebank.domain.account.AccountType;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;


public class AccountCreationDto { //TODO: Validation

  @NotNull
  private AccountType accountType;

  @JsonIgnore
  private String accountNumber;

  @JsonIgnore
  private String currency = "SGD"; //TODO: Property file

  @JsonIgnore
  private BigDecimal balance = BigDecimal.ZERO;

  @JsonIgnore
  private LocalDateTime createDate = LocalDateTime.now();


  public AccountType getAccountType() {
    return accountType;
  }

  public void setAccountType(AccountType accountType) {
    this.accountType = accountType;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public LocalDateTime getCreateDate() {
    return createDate;
  }

  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  @Override
  public String toString() {
    return new StringBuffer("AccountCreationDto [")
      .append("accountType=").append(accountType)
      .append(", accountNumber=").append(accountNumber)
      .append(", currency=").append(currency)
      .append(", balance=").append(balance)
      .append(", createDate=").append(createDate)
      .append("]").toString();
  }
}