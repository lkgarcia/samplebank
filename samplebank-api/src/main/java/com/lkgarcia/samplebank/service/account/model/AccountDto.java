package com.lkgarcia.samplebank.service.account.model;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.lkgarcia.samplebank.domain.account.Account;
import com.lkgarcia.samplebank.domain.account.AccountType;
import org.springframework.beans.BeanUtils;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@JsonInclude(Include.NON_NULL)
public class AccountDto {

  private Long id;
  
  private String accountNumber;

  private AccountType accountType;

  private String currency;

  private BigDecimal balance;

  // private List<TransactionDto> transactions; //TODO: Remove and retrieve as transaction service


  public AccountDto() { }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAccountNumber() {
    return accountNumber;
  }
  
  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }
  
  public AccountType getAccountType() {
    return accountType;
  }
  
  public void setAccountType(AccountType accountType) {
    this.accountType = accountType;
  }
  
  public String getCurrency() {
    return currency;
  }
  
  public void setCurrency(String currency) {
    this.currency = currency;
  }
  
  public BigDecimal getBalance() {
    return balance;
  }
  
  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  // public void setTransactions (List<TransactionDto> transactions) {
  //   this.transactions = transactions;
  // }

  // public List<TransactionDto> getTransactions () {
  //   return transactions;
  // }

  public static AccountDto wrap(Account entity) {
    AccountDto model = new AccountDto();
    BeanUtils.copyProperties(entity, model);
    return model;
  }

  @Override
  public String toString() {
    return new StringBuffer("AccountDto [")
      .append("accountNumber=").append(accountNumber)
      .append(", accountType=").append(accountType)
      .append(", accountNumber=").append(accountNumber)
      .append(", currency=").append(currency)
      .append(", balance=").append(balance)
      // .append(", transactions=").append(transactions)
      .append("]").toString();
  }
}