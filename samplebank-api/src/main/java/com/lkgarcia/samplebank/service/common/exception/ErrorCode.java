package com.lkgarcia.samplebank.service.common.exception;

public enum ErrorCode {
  SERVER_ERROR,
  NOT_FOUND,
  BAD_REQUEST,
  VALIDATION_FAILED,
  ACCESS_DENIED;
}