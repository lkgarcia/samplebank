package com.lkgarcia.samplebank.service.common.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
public class ErrorResponse {
  
  private ErrorCode code;

  private String message;

  private Object details;

  
  public ErrorResponse(ErrorCode code, String message) {
    this(code, message, null);
  }

  public ErrorResponse(ErrorCode code, String message, Object details) {
    this.code = code;
    this.message = message;
    this.details = details;
  }

  public ErrorCode getCode() {
    return code;
  }

  public void setCode(ErrorCode code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
  
  public Object getDetails() {
    return details;
  }
  
  public void setDetails(Object details) {
    this.details = details;
  }  
}