package com.lkgarcia.samplebank.service.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class GlobalControllerAdvice {
  
  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @ExceptionHandler(value = SamplebankException.class)
  public ResponseEntity<Object> handleDefaultException(SamplebankException ex) {
    logger.warn("Expected error occurred. {}", ex.getMessage());
    return new ResponseEntity<>(ex.getResponse(), ex.getStatus());
  }

  @ExceptionHandler(value = MethodArgumentNotValidException.class)
  public ResponseEntity<Object> handleValdationException(MethodArgumentNotValidException ex) {
    logger.warn("Validation exception occurred. {}", ex.getMessage());

    RequestValidationException ex2 = new RequestValidationException(ex);
    return new ResponseEntity<>(ex2.getResponse(), ex2.getStatus());
  }

  

  @ExceptionHandler(value = Exception.class)
  public ResponseEntity<Object> handleOtherException(Exception ex) {
    logger.error("Unexpected error occurred.", ex);

    SamplebankException ex2 = new SamplebankException();
    return new ResponseEntity<>(ex2.getResponse(), ex2.getStatus());
  }
}