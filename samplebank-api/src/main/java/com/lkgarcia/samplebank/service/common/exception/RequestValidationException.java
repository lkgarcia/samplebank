package com.lkgarcia.samplebank.service.common.exception;

import com.lkgarcia.samplebank.service.common.exception.ErrorCode;
import com.lkgarcia.samplebank.service.common.exception.ErrorResponse;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

public class RequestValidationException extends SamplebankException {

  private static final long serialVersionUID = 1329545660545156357L;

  public RequestValidationException(MethodArgumentNotValidException ex) {
    super(HttpStatus.BAD_REQUEST, new ErrorResponse(ErrorCode.VALIDATION_FAILED, "Validation failed for field(s).")); //TODO: Error message from property
    
    // Set details
    List<Map.Entry<String, String>> errorDetails = new ArrayList<>();
    
    for (ObjectError error : ex.getBindingResult().getAllErrors()) {
      if (error instanceof FieldError) {
        errorDetails.add(new AbstractMap.SimpleEntry<String, String>(((FieldError) error).getField(), error.getCode()));
      }
      
    }
    
    this.getResponse().setDetails(errorDetails);
  }
}