package com.lkgarcia.samplebank.service.common.exception;

import org.springframework.http.HttpStatus;

public class ResourceNotFoundException extends SamplebankException {

  private static final long serialVersionUID = 6046100552766026639L;

  public ResourceNotFoundException(Class resourceClass) {
    super(HttpStatus.NOT_FOUND, new ErrorResponse(ErrorCode.NOT_FOUND, new StringBuffer("Resource (").append(resourceClass.getSimpleName()).append(") not found.").toString())); //TODO: Error message from property
  }
}