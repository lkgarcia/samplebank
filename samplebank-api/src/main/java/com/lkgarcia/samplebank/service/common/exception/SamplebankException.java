package com.lkgarcia.samplebank.service.common.exception;

import org.springframework.http.HttpStatus;


public class SamplebankException extends RuntimeException {

	private static final long serialVersionUID = -7130370659287214525L;

  private HttpStatus status;

  private ErrorResponse response;

  //TODO: Create service exception remove web attributes (ex. httpstatus error response)
  public SamplebankException() {
    super("An unexpected server error has occurred. Please try again."); //TODO
    this.status = HttpStatus.INTERNAL_SERVER_ERROR;
    this.response = new ErrorResponse(ErrorCode.SERVER_ERROR, "An unexpected server error has occurred. Please try again."); //TODO: Error message from property
  }

  public SamplebankException(HttpStatus status, ErrorResponse response) {
    super(response.getMessage());
    this.status = status;
    this.response = response;
  }
  
  public HttpStatus getStatus() {
    return status;
  }
  
  public void setStatus(HttpStatus status) {
    this.status = status;
  }
  
  public ErrorResponse getResponse() {
    return response;
  }
  
  public void setResponse(ErrorResponse response) {
    this.response = response;
  }
}