package com.lkgarcia.samplebank.service.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;


public class EntityToDtoConverter<S, T> implements Converter<S, T> {
  
  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  private Class<T> clazz;

  public EntityToDtoConverter(Class<T> clazz) {
        this.clazz = clazz;
    }

	@Override
	public T convert(S source) {
    T target = null;
    
    try {
      target = clazz.newInstance();
      BeanUtils.copyProperties(source, target);
		} catch (Exception ex) {
			logger.error("Unexpected error occurred while converting.", ex);
    } 
    
    return target;
	}

}