package com.lkgarcia.samplebank.service.customer;

import com.lkgarcia.samplebank.domain.customer.Customer;
import com.lkgarcia.samplebank.service.customer.exception.CustomerAlreadyExistException;
import com.lkgarcia.samplebank.service.customer.exception.CustomerNotFoundException;
import com.lkgarcia.samplebank.service.customer.model.CustomerCreationDto;
import com.lkgarcia.samplebank.service.customer.model.CustomerDto;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
public interface CustomerService {

  /**
   * Retrieve customer based on ID.
   */
  public CustomerDto getCustomer(Long id) throws CustomerNotFoundException;

  /**
   * Retrieve customer entity based on ID.
   */
  public Customer getCustomerEntity(Long id) throws CustomerNotFoundException;

  /**
   * Create a new customer.
   */
  public CustomerDto createCustomer(CustomerCreationDto request) throws CustomerAlreadyExistException;
}