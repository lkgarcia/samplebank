package com.lkgarcia.samplebank.service.customer;

import java.time.LocalDateTime;
import com.lkgarcia.samplebank.service.account.AccountService;
import com.lkgarcia.samplebank.service.account.model.AccountCreationDto;
import com.lkgarcia.samplebank.service.customer.exception.CustomerAlreadyExistException;
import com.lkgarcia.samplebank.service.customer.exception.CustomerNotFoundException;
import com.lkgarcia.samplebank.service.customer.model.CustomerCreationDto;
import com.lkgarcia.samplebank.service.customer.model.CustomerDto;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;
import com.lkgarcia.samplebank.domain.account.AccountType;
import com.lkgarcia.samplebank.domain.customer.Customer;
import com.lkgarcia.samplebank.domain.customer.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@Service
public class CustomerServiceImpl implements CustomerService  {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private AccountService accountService;


  /**
   * {@inheritDoc}
   */
  public CustomerDto getCustomer(Long id) throws CustomerNotFoundException {
    logger.debug("Parameter (id): {}", id);

    CustomerDto result = null;

    try {
      Customer entity = customerRepository.findOne(id);
      logger.debug("entity: {}", entity);

      if (entity != null) {
        result = CustomerDto.wrap(entity);

        //TODO: Update last login
      } else {
        logger.warn("Customer ({}) not found.", id);
        throw new CustomerNotFoundException(id);
      }
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }

    logger.debug("Result (CustomerDto): {}", result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  public Customer getCustomerEntity(Long id) throws CustomerNotFoundException {
    logger.debug("Parameter (id): {}", id);

    Customer result = null;

    try {
      result = customerRepository.findOne(id);
      logger.debug("result: {}", result);

      if (result == null) {
        logger.warn("Customer ({}) not found.", id);
        throw new CustomerNotFoundException(id);
      }
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }

    logger.debug("Result (Customer): {}", result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(rollbackFor = Exception.class)
  public CustomerDto createCustomer(CustomerCreationDto request) throws CustomerAlreadyExistException {
    logger.debug("Parameter (request): {}", request);

    CustomerDto result = null;

    try {
      //Validation: Check customer already exists
      boolean byName = customerRepository.findByName(request.getName()) != null;
      boolean byEmail = customerRepository.findByEmail(request.getEmail()) != null;
      logger.debug("Validation - customer exist (byName): {}", byName);
      logger.debug("Validation - customer exist (byEmail): {}", byEmail);

      if (!byName && !byEmail) {
        //Create customer
        Customer entity = new Customer();
        BeanUtils.copyProperties(request, entity);
        entity.setCreateDate(LocalDateTime.now());
        
        //Persist
        entity = customerRepository.save(entity);
        result = CustomerDto.wrap(entity);

        //Create account
        AccountCreationDto createAccountRequest = new AccountCreationDto();
        createAccountRequest.setAccountType(AccountType.SAVINGS);
        accountService.createAccount(entity.getId(), createAccountRequest);
        
      } else {
        logger.warn("Customer already exists.");
        throw new CustomerAlreadyExistException();
      }

    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }

    return result;
  }
}