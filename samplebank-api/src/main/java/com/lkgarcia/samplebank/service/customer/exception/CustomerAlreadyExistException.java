package com.lkgarcia.samplebank.service.customer.exception;

import com.lkgarcia.samplebank.service.common.exception.ErrorCode;
import com.lkgarcia.samplebank.service.common.exception.ErrorResponse;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;

import org.springframework.http.HttpStatus;

public class CustomerAlreadyExistException extends SamplebankException {

  private static final long serialVersionUID = 7407124001365968620L;

  public CustomerAlreadyExistException() {
    super(HttpStatus.BAD_REQUEST, new ErrorResponse(ErrorCode.VALIDATION_FAILED, "Customer already exists.")); //TODO: Error message from property
  }
}