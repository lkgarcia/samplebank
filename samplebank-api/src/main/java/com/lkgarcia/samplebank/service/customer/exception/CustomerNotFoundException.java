package com.lkgarcia.samplebank.service.customer.exception;

import com.lkgarcia.samplebank.service.common.exception.ErrorCode;
import com.lkgarcia.samplebank.service.common.exception.ErrorResponse;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;

import org.springframework.http.HttpStatus;

public class CustomerNotFoundException extends SamplebankException {

	private static final long serialVersionUID = 2745497080379214742L;

  public CustomerNotFoundException(Long id) {
    super(HttpStatus.NOT_FOUND, new ErrorResponse(ErrorCode.NOT_FOUND, new StringBuffer("Customer (").append(id).append(") not found.").toString())); //TODO: Error message from property
  }
}