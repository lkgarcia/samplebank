package com.lkgarcia.samplebank.service.customer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class CustomerCreationDto {
  
  @NotEmpty
  @Size(max = 100)
  private String name;

  @NotEmpty @Email
  @Size(max = 100)
  private String email;

  @NotNull
  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDate birthDate;

  @JsonIgnore
  private LocalDateTime createDate = LocalDateTime.now();

  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public LocalDateTime getCreateDate() {
    return createDate;
  }
  
  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  @Override
  public String toString() {
    return new StringBuffer("CreateCustomerRequest [")
      .append("name=").append(name)
      .append(", email=").append(email)
      .append(", birthDate=").append(birthDate)
      .append(", createDate=").append(createDate)
      .append("]").toString();
  }
}