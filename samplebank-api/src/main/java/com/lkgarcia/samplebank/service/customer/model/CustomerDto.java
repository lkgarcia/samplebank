package com.lkgarcia.samplebank.service.customer.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.lkgarcia.samplebank.domain.customer.Customer;
import java.time.LocalDate;
import org.springframework.beans.BeanUtils;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@JsonInclude(Include.NON_NULL)
public class CustomerDto {

  private Long id;

  private String name;

  private String email;

  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDate birthDate;


  public CustomerDto() { }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public static CustomerDto wrap(Customer entity) {
    CustomerDto model = new CustomerDto();
    BeanUtils.copyProperties(entity, model);

    // List<AccountDto> acctsModel = new ArrayList<>();

    // for (Account acctEntity : entity.getAccounts()) {
    //   AccountDto acctModel = new AccountDto();
    //   BeanUtils.copyProperties(acctEntity, acctModel);
    //   acctsModel.add(acctModel);
    // }

    // model.setAccounts(acctsModel);

    return model;
  }

  @Override
  public String toString() {
    return new StringBuffer("CustomerDto [")
      .append("name=").append(name)
      .append(", email=").append(email)
      .append(", birthDate=").append(email)
      .append("]").toString();
  }
}