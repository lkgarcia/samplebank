package com.lkgarcia.samplebank.service.transaction;

import com.lkgarcia.samplebank.domain.transaction.TransactionType;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import org.springframework.stereotype.Component;

@Component
public class TransactionNumberGenerator {
  private Random random = new SecureRandom();
  private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd-HHmmssSSS");

  /**
   * Example: SBTRCR180226-111500999-1234
   */
  public String generate(TransactionType type, LocalDateTime createDate) {
    return new StringBuffer("SBTR").append(type.getValue()).append(createDate.format(formatter)).append("-").append(String.format("%04d", random.nextInt(10000))).toString();
  }
}