package com.lkgarcia.samplebank.service.transaction;

import com.lkgarcia.samplebank.service.transaction.model.TransactionDto;
import org.springframework.data.domain.Page;
import com.lkgarcia.samplebank.service.transaction.exception.InsufficientAccountBalanceException;
import com.lkgarcia.samplebank.service.transaction.model.TransactionCreationDto;
import com.lkgarcia.samplebank.service.account.exception.AccountNotFoundException;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
public interface TransactionService {
  /**
   * Retrieve page list of accounts based on customer ID.
   */
  public Page<TransactionDto> getTransactions(Long accountId, int page) throws AccountNotFoundException;

  /**
   * Create a new transaction.
   */
  public TransactionDto createTransaction(Long accountId, TransactionCreationDto request) throws AccountNotFoundException, InsufficientAccountBalanceException;
}