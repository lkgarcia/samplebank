package com.lkgarcia.samplebank.service.transaction;

import com.lkgarcia.samplebank.service.transaction.model.TransactionDto;
import com.lkgarcia.samplebank.service.common.utils.EntityToDtoConverter;
import java.math.BigDecimal;
import com.lkgarcia.samplebank.service.transaction.exception.InsufficientAccountBalanceException;
import com.lkgarcia.samplebank.service.transaction.model.TransactionCreationDto;
import com.lkgarcia.samplebank.service.account.AccountService;
import com.lkgarcia.samplebank.service.account.exception.*;
import com.lkgarcia.samplebank.service.common.exception.ResourceNotFoundException;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;
import com.lkgarcia.samplebank.domain.transaction.Transaction;
import com.lkgarcia.samplebank.domain.transaction.TransactionRepository;
import com.lkgarcia.samplebank.domain.transaction.TransactionType;
import com.lkgarcia.samplebank.domain.account.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@Service
public class TransactionServiceImpl implements TransactionService  {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private TransactionRepository transactionRepository;

  @Autowired
  private AccountService accountService;

  @Autowired
  private TransactionNumberGenerator generator;

  private final static int PAGE_SIZE = 10; //TODO: Properties


  /**
   * {@inheritDoc}
   */
  public Page<TransactionDto> getTransactions(Long accountId, int page) throws AccountNotFoundException {
    logger.debug("Parameter (accountId): {}", accountId);
    logger.debug("Parameter (page): {}", page);
    
    Page<TransactionDto> result = null;

    try {
      Page<Transaction> entityList = transactionRepository.findByAccountId(accountId, new PageRequest(page, PAGE_SIZE, Direction.DESC, "createDate"));
      logger.debug("entityList: {}", entityList);
      
      if (entityList != null && page < entityList.getTotalPages()) {
        result = entityList.map(new EntityToDtoConverter<Transaction, TransactionDto>(TransactionDto.class));
      } else {
        logger.warn("Resource (Transaction) not found.");
        throw new ResourceNotFoundException(Transaction.class);
      } 
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }

    logger.debug("Result (List<TransactionDto>): {}", result);
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(rollbackFor = Exception.class)
  public TransactionDto createTransaction(Long accountId, TransactionCreationDto request) throws AccountNotFoundException, InsufficientAccountBalanceException {
    logger.debug("Parameter (accountId): {}", accountId);
    logger.debug("Parameter (request): {}", request);

    TransactionDto result = null;

    try {
      //Check if account exists
      Account account = accountService.getAccountEntity(accountId);
      
      if (account != null) {
        //Check balance
        BigDecimal newBalance =  (request.getTransactionType() == TransactionType.DEBIT) ? account.getBalance().subtract(request.getAmount()) : account.getBalance().add(request.getAmount());
        if (newBalance.compareTo(BigDecimal.ZERO) >= 0) {
       
          //Create transaction
          Transaction transaction = new Transaction();
          BeanUtils.copyProperties(request, transaction);
          transaction.setTransactionNumber(generator.generate(transaction.getTransactionType(), transaction.getCreateDate()));
          transaction.setAccount(account);
          logger.debug("transaction: {}", transaction);
          
          //Persist
          transactionRepository.save(transaction);
          result = TransactionDto.wrap(transaction);

          //Update Account Balance
          accountService.updateBalance(account.getId(), transaction.getTransactionType(), transaction.getAmount());
        } else {
          throw new InsufficientAccountBalanceException(account.getBalance(), request.getAmount());
        }
      } else {
        logger.warn("Account ({}) not found.", accountId);
        throw new AccountNotFoundException(accountId);
      }
    } catch (SamplebankException ex) {
      logger.warn("Expected error occurred. {}", ex.getMessage());
      throw ex;
    } catch (Exception ex) {
      logger.warn("Unexpected error occurred. {}", ex.getMessage());
      throw ex;
    }

    logger.debug("Result (TransactionDto): {}", result);
    return result;
  }
}