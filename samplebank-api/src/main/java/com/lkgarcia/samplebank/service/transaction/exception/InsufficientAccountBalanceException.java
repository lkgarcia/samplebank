package com.lkgarcia.samplebank.service.transaction.exception;

import java.math.BigDecimal;

import com.lkgarcia.samplebank.service.common.exception.ErrorCode;
import com.lkgarcia.samplebank.service.common.exception.ErrorResponse;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;

import org.springframework.http.HttpStatus;

public class InsufficientAccountBalanceException extends SamplebankException {

  private static final long serialVersionUID = -1769709798842916674L;

  public InsufficientAccountBalanceException(BigDecimal balance, BigDecimal amount) {
    super(HttpStatus.BAD_REQUEST, new ErrorResponse(ErrorCode.BAD_REQUEST, new StringBuffer("Insufficient account balance (").append(balance).append("). Unable to deduct amount (").append(amount).append(").").toString())); //TODO: Error message from property
  }
}