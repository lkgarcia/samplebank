package com.lkgarcia.samplebank.service.transaction.exception;

import com.lkgarcia.samplebank.service.common.exception.ErrorCode;
import com.lkgarcia.samplebank.service.common.exception.ErrorResponse;
import com.lkgarcia.samplebank.service.common.exception.SamplebankException;

import org.springframework.http.HttpStatus;

public class TransactionNotFoundException extends SamplebankException {

  private static final long serialVersionUID = -3465535578593411199L;

  public TransactionNotFoundException(Long id) {
    super(HttpStatus.NOT_FOUND, new ErrorResponse(ErrorCode.NOT_FOUND, new StringBuffer("Transaction (").append(id).append(") not found.").toString())); //TODO: Error message from property
  }
}