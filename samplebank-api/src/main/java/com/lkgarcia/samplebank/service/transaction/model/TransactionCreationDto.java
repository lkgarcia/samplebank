package com.lkgarcia.samplebank.service.transaction.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lkgarcia.samplebank.domain.transaction.TransactionType;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;


public class TransactionCreationDto { //TODO: Validation

  @JsonIgnore
  private String transactionNumber;

  @NotEmpty
  private String description;

  @NotNull
  private TransactionType transactionType;
  
  @NotNull
  @Min(1)
  private BigDecimal amount = BigDecimal.ZERO;

  @JsonIgnore
  private LocalDateTime createDate = LocalDateTime.now();


  public String getTransactionNumber() {
    return transactionNumber;
  }

  public void setTransactionNumber(String transactionNumber) {
    this.transactionNumber = transactionNumber;
  }

  public TransactionType getTransactionType() {
    return transactionType;
  }
  
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setTransactionType(TransactionType transactionType) {
    this.transactionType = transactionType;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public LocalDateTime getCreateDate() {
    return createDate;
  }

  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  @Override
  public String toString() {
    return new StringBuffer("TransactionCreationDto [")
      .append("transactionNumber=").append(transactionNumber)
      .append(", description=").append(description)
      .append(", transactionType=").append(transactionType)
      .append(", amount=").append(amount)
      .append(", createDate=").append(createDate)
      .append("]").toString();
  }
}