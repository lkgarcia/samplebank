package com.lkgarcia.samplebank.service.transaction.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.lkgarcia.samplebank.domain.transaction.Transaction;
import com.lkgarcia.samplebank.domain.transaction.TransactionType;

import org.springframework.beans.BeanUtils;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@JsonInclude(Include.NON_NULL)
public class TransactionDto {

  private Long id;
  
  private String transactionNumber;

  private String description;

  private TransactionType transactionType;

  private BigDecimal amount;

  @JsonFormat(pattern = "yyyy-MM-dd")
  private LocalDateTime createDate;


  public TransactionDto() { }
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
  
  public String getTransactionNumber() {
    return transactionNumber;
  }
  
  public void setTransactionNumber(String transactionNumber) {
    this.transactionNumber = transactionNumber;
  }
  
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
  
  public TransactionType getTransactionType() {
    return transactionType;
  }
  
  public void setTransactionType(TransactionType transactionType) {
    this.transactionType = transactionType;
  }
  
  public BigDecimal getAmount() {
    return amount;
  }
  
  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public LocalDateTime getCreateDate() {
    return createDate;
  }

  public void setCreateDate(LocalDateTime createDate) {
    this.createDate = createDate;
  }

  public static TransactionDto wrap(Transaction entity) {
    TransactionDto model = new TransactionDto();
    BeanUtils.copyProperties(entity, model);
    return model;
  }

  @Override
  public String toString() {
    return new StringBuffer("TransactionDto [")
      .append("transactionNumber=").append(transactionNumber)
      .append(", description=").append(description)
      .append(", transactionType=").append(transactionType)
      .append(", transactionNumber=").append(transactionNumber)
      .append(", amount=").append(amount)
      .append(", createDate=").append(createDate)
      .append("]").toString();
  }
}