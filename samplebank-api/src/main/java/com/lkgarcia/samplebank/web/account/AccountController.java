package com.lkgarcia.samplebank.web.account;

import com.lkgarcia.samplebank.service.account.AccountService;
import com.lkgarcia.samplebank.service.account.model.AccountDto;
import com.lkgarcia.samplebank.service.account.model.AccountCreationDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/samplebank/api/v1/customers/{customerId}/accounts")
public class AccountController {

  @Autowired
  private AccountService accountService;

  //TODO: Controller parameters validation
  @GetMapping(value = "/{accountId}")
  public AccountDto getAccount(@PathVariable Long customerId, @PathVariable Long accountId) {
    return accountService.getAccount(customerId, accountId);
  }

  @GetMapping
  public Page<AccountDto> getAccounts(@PathVariable Long customerId, @RequestParam(value = "page", required = false) Integer page) {
    page = (page == null) ? 0 : page.intValue();
    return accountService.getAccounts(customerId, page);
  }

  @PostMapping
  public AccountDto createAccount(@PathVariable Long customerId, @Validated @RequestBody AccountCreationDto request) {
    return accountService.createAccount(customerId, request);
  }
}