package com.lkgarcia.samplebank.web.customer;

import com.lkgarcia.samplebank.service.customer.CustomerService;
import com.lkgarcia.samplebank.service.customer.model.CustomerCreationDto;
import com.lkgarcia.samplebank.service.customer.model.CustomerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/samplebank/api/v1/customers")
 public class CustomerController {

  @Autowired
  private CustomerService customerService;


  @GetMapping(value = "/{id}")
  public CustomerDto getCustomer(@PathVariable Long id) {
    //TODO: Encoded Customer ID
    //TODO: Auth Token
    return customerService.getCustomer(id);
  }

  @PostMapping
  public CustomerDto createCustomer(@Validated @RequestBody CustomerCreationDto customer) {
    return customerService.createCustomer(customer);
  }
}