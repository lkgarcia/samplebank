package com.lkgarcia.samplebank.web.transaction;

import com.lkgarcia.samplebank.service.transaction.TransactionService;
import com.lkgarcia.samplebank.service.transaction.model.TransactionDto;
import com.lkgarcia.samplebank.service.transaction.model.TransactionCreationDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author lkgarcia (nicolo.kevin@gmail.com)
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/samplebank/api/v1/customers/{customerId}/accounts/{accountId}/transactions") //TODO: Do I need customerId?
public class TransactionController {

  @Autowired
  private TransactionService transactionService;

  
  @GetMapping()
  public Page<TransactionDto> getAccounts(@PathVariable Long customerId, @PathVariable Long accountId, @RequestParam(value = "page", required = false) Integer page) {
    page = (page == null) ? 0 : page.intValue();
    return transactionService.getTransactions(accountId, page);
  }

  @PostMapping()
  public TransactionDto createTransaction(@PathVariable Long customerId, @PathVariable Long accountId, @Validated @RequestBody TransactionCreationDto request) {
    return transactionService.createTransaction(accountId, request);
  }
}