package com.lkgarcia.samplebank.service.account.model;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.lkgarcia.samplebank.domain.account.Account;
import com.lkgarcia.samplebank.domain.account.AccountType;

import org.apache.tomcat.jni.Local;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AcountDtoTest {
  
  @Test
  public void wrap() {
    LocalDateTime now = LocalDateTime.now();

    Account entity = new Account();
    entity.setId(Long.valueOf(1));
    entity.setAccountNumber("1");
    entity.setAccountType(AccountType.SAVINGS);
    entity.setBalance(BigDecimal.ZERO);
    entity.setCurrency("SGD");

    AccountDto dto = AccountDto.wrap(entity);

    assertEquals(dto.getId(), entity.getId());
    assertEquals(dto.getAccountNumber(), entity.getAccountNumber());
    assertEquals(dto.getAccountType(), entity.getAccountType());
    assertEquals(dto.getCurrency(), entity.getCurrency());
    assertEquals(dto.getBalance(), entity.getBalance());
  }
}