package com.lkgarcia.samplebank.web.customer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import com.lkgarcia.samplebank.SamplebankApplication;
import com.lkgarcia.samplebank.domain.customer.CustomerRepository;
import com.lkgarcia.samplebank.service.customer.CustomerService;
import com.lkgarcia.samplebank.service.customer.exception.CustomerAlreadyExistException;
import com.lkgarcia.samplebank.service.customer.model.CustomerCreationDto;
import com.lkgarcia.samplebank.service.customer.model.CustomerDto;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SamplebankApplication.class)
@AutoConfigureMockMvc
public class CustomerControllerIntTest {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private CustomerRepository repository;
  
  @Autowired
  private CustomerService service;

  @Test
  @Transactional
  public void createCustomer() throws Exception {
    CustomerCreationDto request = new CustomerCreationDto() {{
      setName("GARCIA KEVIN");
      setEmail("NICOLO.KEVIN@GMAIL.COM");
      setBirthDate(LocalDate.of(1987, 2, 26));
    }};
    
    CustomerDto dto = service.createCustomer(request);
    logger.debug("dto: {}", dto);

    assertNotNull(dto);
    assertNotNull(dto.getId());
    assertEquals(request.getName(), dto.getName());
    assertEquals(request.getEmail(), dto.getEmail());
    assertEquals(request.getBirthDate(), dto.getBirthDate());
  }

  @Test(expected = CustomerAlreadyExistException.class)
  @Transactional
  public void createDuplicateCustomer() throws Exception {
    CustomerCreationDto request = new CustomerCreationDto() {{
      setName("GARCIA KEVIN");
      setEmail("NICOLO.KEVIN@GMAIL.COM");
      setBirthDate(LocalDate.of(1987, 2, 26));
    }};
    
    CustomerDto dto = service.createCustomer(request);
    dto = service.createCustomer(request);
  }
}