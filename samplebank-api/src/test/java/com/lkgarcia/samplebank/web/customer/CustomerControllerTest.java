package com.lkgarcia.samplebank.web.customer;

import java.time.LocalDate;

import com.lkgarcia.samplebank.service.customer.CustomerService;
import com.lkgarcia.samplebank.service.customer.model.CustomerDto;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CustomerController.class, secure = false)

public class CustomerControllerTest {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private CustomerService service;

  CustomerDto mockCustomer = new CustomerDto(){{
    setId(Long.valueOf(1));
    setName("GARCIA KEVIN");
    setEmail("NICOLO.KEVIN@GMAIL.COM");
    setBirthDate(LocalDate.of(1987, 2, 26));
  }};


  @Test
  public void getCustomer() throws Exception {
    Mockito.when(service.getCustomer(Mockito.anyLong())).thenReturn(mockCustomer);
    RequestBuilder rb = MockMvcRequestBuilders.get("/samplebank/api/v1/customers/1").accept(MediaType.APPLICATION_JSON_VALUE);
    
    MvcResult result = mockMvc.perform(rb).andReturn();
    logger.debug("result: {}", result);

    String expected = "{id:1,name:\"GARCIA KEVIN\",email:\"NICOLO.KEVIN@GMAIL.COM\",birthDate:\"1987-02-26\"}";
    JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
  }

  @Test
  public void createCustomer() throws Exception {
    // Mockito.when(service.getCustomer(Mockito.anyLong())).thenReturn(mockCustomer);
    // RequestBuilder rb = MockMvcRequestBuilders.get("/samplebank/api/v1/customers/1").accept(MediaType.APPLICATION_JSON_VALUE);
    
    // MvcResult result = mockMvc.perform(rb).andReturn();
    // logger.debug("result: {}", result);

    // String expected = "{id:1,name:GARCIA KEVIN,email:NICOLO.KEVIN@GMAIL.COM,birthdate:1987-02-26}";
    // JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
  }
}