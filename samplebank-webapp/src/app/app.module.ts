import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { AccountDetailComponent } from './components/account-detail/account-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { CustomerService } from './services/customer.service';
import { AccountService } from './services/account.service';
import { TransactionService } from './services/transaction.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AccountsComponent,
    AccountDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    RouterModule,
  ],
  providers: [
    CustomerService,
    AccountService,
    TransactionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
