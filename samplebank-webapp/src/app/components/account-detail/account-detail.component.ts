import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Account } from '../../models/account';
//import { PAGE_ACCOUNTS } from '../../models/pageAccount-mock';
import { Page } from '../../models/page';
import { Transaction } from '../../models/transaction';
// import { PAGE_TRANSACTIONS } from '../../models/pageTransaction-mock';
import { AccountService } from '../../services/account.service';
import { TransactionService } from '../../services/transaction.service';


@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.css']
})
export class AccountDetailComponent implements OnInit {

  lastPage:boolean = true;
  page:number = 0;
  customerId:string = "1"; //TODO: Retrieve from path variable
  accountId:string;
  account:Account;// = PAGE_ACCOUNTS.content[0];
  transactions:Transaction[];// = PAGE_TRANSACTIONS.content;

  constructor(private transactionService:TransactionService, private accountService:AccountService, private route: ActivatedRoute) {
    this.accountId = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.accountService.getAccount(this.customerId, this.accountId).subscribe(
      (account) => {
        this.account = account;
      },
      (error) => {
        console.log(error);
      }
    );
    
    this.transactionService.getTransactions(this.accountId, this.page).subscribe(
      (page) => {
        this.transactions = page.content;
        this.lastPage = page.last;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  loadMore() {
    this.page = this.page + 1;
    this.transactionService.getTransactions(this.accountId, this.page).subscribe(
      (page) => {
        [].push.apply(this.transactions, page.content);
        this.lastPage = page.last;
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
