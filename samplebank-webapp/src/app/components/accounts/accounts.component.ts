import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Account } from '../../models/account';
import { Page } from '../../models/page';
// import { PAGE_ACCOUNTS } from '../../models/pageAccount-mock';

import { AccountService } from '../../services/account.service';


@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  
  lastPage:boolean = true;
  page:number = 0;
  customerId:string = "1"; //TODO: Retrieve from path variable
  accounts:Account[];// = PAGE_ACCOUNTS.content;

  constructor(private service:AccountService, private router: Router) { }

  ngOnInit() {
    this.service.getAccounts(this.customerId, this.page).subscribe(
      (page) => {
        this.accounts = page.content;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onClick(account:Account): void {
    console.log("account", account);
    this.router.navigate(['accounts/' + account.id]);
  }

  loadMore() {
    this.page = this.page + 1;
    this.service.getAccounts(this.customerId, this.page).subscribe(
      (page) => {
        [].push.apply(this.accounts, page.content);
        this.lastPage = page.last;
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
