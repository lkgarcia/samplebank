import { Component, OnInit, Input } from '@angular/core';

import { Customer } from '../../models/customer';
//import { CUSTOMER } from '../../models/customer-mock';

import { CustomerService } from '../../services/customer.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input()
  customerId:string;
  customer:Customer;//  = CUSTOMER;

  constructor(private service:CustomerService) { }

  ngOnInit() {
    //TODO: retrieve from path variable
    this.service.getCustomer(this.customerId).subscribe(
      (customer) => {
        this.customer = customer;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onProfileClick() {
    console.log("TODO: Handle profile click event.");
  }

}
