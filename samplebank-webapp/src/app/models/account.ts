import { AccountType } from './enumAccountType';

export class Account {
  id: number;
  accountNumber: string;
  accountType: AccountType;
  currency: string;
  balance: number;
}