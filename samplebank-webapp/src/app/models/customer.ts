export class Customer {
  id: number;
  name: string;
  email: string;
  birthDate: string;
}