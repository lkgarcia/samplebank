export class Page {
  content: any[];
  totalPages: number;
  last: boolean;
  totalElements: number;
  size: number;
  number: number;
  first: boolean;
  numberOfElements: number;
}