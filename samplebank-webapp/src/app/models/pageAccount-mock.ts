import { Page } from './page';
import { AccountType } from './enumAccountType';

export const PAGE_ACCOUNTS: Page = {
  "content": [
    {
      "id": 1,
      "accountNumber": "100-1-65807",
      "accountType": AccountType.SAVINGS,
      "currency": "SGD",
      "balance": 584.95
    },
    {
      "id": 2,
      "accountNumber": "100-2-81399",
      "accountType": AccountType.CURRENT,
      "currency": "SGD",
      "balance": 1472.55
    }
  ],
  "totalPages": 1,
  "last": true,
  "totalElements": 2,
  "size": 5,
  "number": 0,
  "first": true,
  "numberOfElements": 2
};