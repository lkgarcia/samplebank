import { Page } from './page';
import { TransactionType } from './enumTransactionType';

export const PAGE_TRANSACTIONS: Page = {
  "content": [
      {
        "id": 1,
        "transactionNumber": "SBTRCR180228-215323071-1537",
        "description": "Salary",
        "transactionType": TransactionType.CREDIT,
        "amount": 100,
        "createDate": "2018-02-28"
      },
      {
        "id": 2,
        "transactionNumber": "SBTRCR180228-215450738-8363",
        "description": "Fund Transfer",
        "transactionType": TransactionType.CREDIT,
        "amount": 500,
        "createDate": "2018-02-28"
      },
      {
        "id": 3,
        "transactionNumber": "SBTRDR180228-215616655-3593",
        "description": "Fund Transfer",
        "transactionType": TransactionType.DEBIT,
        "amount": 15.05,
        "createDate": "2018-02-28"
      },
      {
        "id": 4,
        "transactionNumber": "SBTRCR180228-215822394-6044",
        "description": "Fund Transfer",
        "transactionType": TransactionType.CREDIT,
        "amount": 1500,
        "createDate": "2018-02-28"
      },
      {
        "id": 5,
        "transactionNumber": "SBTRDR180228-215901770-3384",
        "description": "Fund Transfer",
        "transactionType": TransactionType.DEBIT,
        "amount": 1500,
        "createDate": "2018-02-28"
      }
  ],
  "totalPages": 1,
  "last": true,
  "totalElements": 5,
  "size": 10,
  "number": 0,
  "first": true,
  "numberOfElements": 5
};