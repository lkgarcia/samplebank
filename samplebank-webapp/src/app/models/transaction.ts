import { TransactionType } from './enumTransactionType';

export class Transaction {
  id: number;
  transactionNumber: string;
  description: string;
  transactionType: TransactionType;
  amount: number;
  createDate: string;
}