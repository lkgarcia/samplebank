import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { Account } from '../models/account';
import { Page } from '../models/page';


@Injectable()
export class AccountService {

  constructor(public http:Http) { }

  getAccount(customerId:any, accountId:any) {
    return this.http.get('/samplebank/api/v1/customers/' + customerId + '/accounts/' + accountId)
      .map(res => <Account>res.json())
      .catch(this.handleError);
  }

  getAccounts(customerId:any, page:number) {
    return this.http.get('/samplebank/api/v1/customers/' + customerId + '/accounts?page=' + page)
      .map(res => <Page>res.json())
      .catch(this.handleError);
  }

  private handleError(error: any) { 
    let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(error);
  }

}
