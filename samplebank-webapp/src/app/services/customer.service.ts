import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { Customer } from '../models/customer';


@Injectable()
export class CustomerService {

  constructor(public http:Http) { }

  getCustomer(customerId:any) {
    return this.http.get('/samplebank/api/v1/customers/' + customerId)
      .map(res => <Customer>res.json())
      .catch(this.handleError);
  }

  private handleError(error: any) { 
    let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(error);
  }
}
