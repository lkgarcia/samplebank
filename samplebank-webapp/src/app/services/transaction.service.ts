import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { Page } from '../models/page';


@Injectable()
export class TransactionService {

  constructor(public http:Http) { }

  getTransactions(accountId:any, page:number) {
    return this.http.get('/samplebank/api/v1/customers/1/accounts/' + accountId + '/transactions?page=' + page)
      .map(res => <Page>res.json())
      .catch(this.handleError);
  }

  private handleError(error: any) { 
    let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(error);
  }

}
